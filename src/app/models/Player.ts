export interface Player {
  id: string,
  username?: string,
  isHuman: boolean,
  isPatientZero?: boolean
  state?: string
  biteCode?: string
}
