import {Player} from "./Player";

export interface Game {
  id: string,
  name?: string,
  state?: string,
  date?: string,
  players?: number[]
  kills?: number[]
  squads?: number[]
}
