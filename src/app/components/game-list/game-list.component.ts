import { Component, OnInit } from '@angular/core';
import {Game} from "../../models/Game";
import {GameService} from "../../services/game.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-game-list',
  templateUrl: './game-list.component.html',
  styleUrls: ['./game-list.component.scss']
})
export class GameListComponent implements OnInit {

  public games: Game[] = [];

  constructor(private gameService: GameService, private router: Router) { }

  ngOnInit(): void {
    this.gameService.getGames().subscribe({
      next: (gamesFromServer: Game[]) => {
        this.games = gamesFromServer;
        console.log(this.games);
      },
      error: (e) => console.log(e)
    })

  }

  // ngClass: CSS classes added/removed per current state of game
  setCurrentClasses(game: Game): Record<string, boolean> {
    return {
      card: true,
      "mb-3": true,
      "mb-md-2": true,
      "bg-success": game.state=== 'Registration',
      "bg-danger": game.state === 'In Progress',
      "bg-dark":  game.state === 'Complete',
      "text-white":  game.state === 'Complete'
    };
  }

}
