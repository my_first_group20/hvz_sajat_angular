import {Component, Input, OnInit} from '@angular/core';
import { Loader } from '@googlemaps/js-api-loader';
import { mapstyles } from './mapstyles';
import {Game} from "../../models/Game";

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.scss'],
})
export class MapComponent implements OnInit {
  private map: google.maps.Map | undefined;
  @Input() public game?: Game;

  ngOnInit(): void {
    // Load the google map on the browser
    let loader = new Loader({
      apiKey: 'AIzaSyDFxFuZsbQIetcV3CBRKzxB0kHjCxbvwoU',
    });

    loader.load().then(() => {
      console.log('Map is loaded');

      // Location of the Game
      // Static location - Budapest
      const location = {
        lat: 47.497913,
        lng: 19.040236,
      };

      // Map
      this.map = new google.maps.Map(document.getElementById('map')!, {
        center: location,
        zoom: 10,
        styles: mapstyles,
      });

      // Marker
      const marker = new google.maps.Marker({
        position: location,
        map: this.map,
        animation: window.google.maps.Animation.DROP,
      });

      // InfoWindow
      const contentString = `<div>
      <h1 class="title">Game title</h1>
      <p>Start date: </p>
      <p>Number of players: </p>
      </div>`;

      const infoWindow = new google.maps.InfoWindow({
        content: contentString,
      });

      // Open the window
      marker.addListener('click', () => {
        infoWindow.open({
          anchor: marker,
          shouldFocus: false,
        });
      });

      // Close the window
      this.map.addListener('click', () => {
        if (infoWindow) {
          infoWindow.close();
        }
      });
    });
  }
  title = 'google-maps';
}
