import { Component, OnInit } from '@angular/core';
import keycloak from "../../../keycloak";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  constructor() { }

  get isLoggedIn(): Boolean | undefined {
    return keycloak.authenticated
  }

  get name(): string | undefined {
    return keycloak.tokenParsed?.name       //ha encoded info-t akarunk belőle, tokenParsed
  }

  handleLogin(): void {
    keycloak.login();
    // console.log(keycloak.token)
  }

  handleLogout(): void {
    keycloak.logout()
  }

  ngOnInit(): void {
  }

}
