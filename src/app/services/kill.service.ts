import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Player} from "../models/Player";
import {Kill} from "../models/Kill";

@Injectable({
  providedIn: 'root'
})
export class KillService {

  private readonly GAME_URL: string = environment.GAME_URL;

  constructor(private http: HttpClient) { }

  public getAllKillsInGame(gameId: string): Observable<Kill[]> {
    return this.http.get<Kill[]>(`${this.GAME_URL}/${gameId}/kill`);
  }

  public getKillById(gameId: string, killId: string): Observable<Kill> {
    return this.http.get<Kill>(`${this.GAME_URL}/${gameId}/kill/${killId}`);
  }

  public registerKill(gameId: string, killer: Player, biteCode: string): Observable<Kill> {
    return this.http.post<Kill>(`${this.GAME_URL}/${gameId}/kill`, {killerId: killer.id, biteCode: biteCode})
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public updateKill(gameId: string, kill: Kill): Observable<Kill> {
    return this.http.put<Kill>(`${this.GAME_URL}/${gameId}/kill/${kill.id}`, kill)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public patchKill(gameId: string, kill: Kill, body: any): Observable<Kill> {
    return this.http.put<Kill>(`${this.GAME_URL}/${gameId}/kill/${kill.id}`, body)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public deleteKill(gameId: string, killId: string): Observable<Object> {
    return this.http.delete<Object>(`${this.GAME_URL}/${gameId}/kill/${killId}`)
    // todo backenről: Üres objektumot ad vissza
  }
}
