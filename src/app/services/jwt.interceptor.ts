import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import keycloak from "../../keycloak";

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    // Not authenticated. Send request as is.
    if (!keycloak.authenticated || !keycloak.token) {
      return next.handle(request);
    }

    const { token } = keycloak;

    const authRequest = request.clone({
      headers: request.headers.set("Authorization", `Bearer ${token}`),
    });

    return next.handle(authRequest);

  }
}
