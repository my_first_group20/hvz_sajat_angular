import { Injectable } from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Game} from "../models/Game";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private readonly GAME_URL: string = environment.GAME_URL;

  constructor(private http: HttpClient) { }

  public getGames(): Observable<Game[]> {
    return this.http.get<Game[]>(this.GAME_URL);
  }

  public getGame(gameId: string): Observable<Game> {
    return this.http.get<Game>(`${this.GAME_URL}/${gameId}`);
  }

  public saveGame(game: Game): Observable<Game> {
    return this.http.post<Game>(this.GAME_URL, game)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public updateGame(game: Game): Observable<Game> {
    return this.http.put<Game>(`${this.GAME_URL}/${game.id}`, game)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public patchGame(game: Game, body: any): Observable<Game> {
    return this.http.put<Game>(`${this.GAME_URL}/${game.id}`, body)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public deleteGame(gameId: string): Observable<Object> {
    return this.http.delete<Object>(`${this.GAME_URL}/${gameId}`)
    // todo backenről: Üres objektumot ad vissza
  }
}
