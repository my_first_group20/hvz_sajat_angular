import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs";
import {Game} from "../../models/Game";
import {ActivatedRoute, Router} from "@angular/router";
import {GameService} from "../../services/game.service";
import {Player} from "../../models/Player";
import {Kill} from "../../models/Kill";
import {Squad} from "../../models/Squad";
import {FormControl, FormGroup, Validators} from "@angular/forms";

@Component({
  selector: 'app-game-details',
  templateUrl: './game-details.page.html',
  styleUrls: ['./game-details.page.scss']
})
export class GameDetailsPage implements OnInit {

  public idReadFromRoute?: any;
  public subscription?: Subscription;
  public game?: Game;
  public player?: Player;
  public allPlayers?: Player[];
  public allSquads?: Squad[];
  public allKills?: Kill[];

  public killForm: FormGroup = new FormGroup({
    biteCode: new FormControl("", Validators.required),
    killStory: new FormControl("")
  })


  constructor(private activatedRoute: ActivatedRoute,
              private gameService: GameService,
              private router: Router) { }

  ngOnInit(): void {

    this.subscription = this.activatedRoute.paramMap.subscribe({
      next: (param) => {this.idReadFromRoute = param.get("gameId")},
      error: (e) => {console.log(e)}
    })    //subs vége

    //todo: comment-out when api enpoints available:

    // this.gameService.getGame(this.idReadFromRoute).subscribe({
    //   next: (game) => {this.game = game},
    //   error: (e) => {console.log(e)}
    // })

    //todo: temporary game, player object, remove when api enpoints available
    this.game = {
      id: "1",
      name: "Game1 burnt in",
      state: "Registration",
      players: [1, 2, 3]
    }

    this.player = {
      id: "1",
      username: "Player1 burnt in",
      isHuman: true,
      isPatientZero: false,
      biteCode: "abc123"
    }
    //todo: remove till here
  }

  onKillSubmit() {
    console.log("Kill registered")
  }

  joinGame() {
    this.player = {
      id: "1",
      username: "Player1 burnt in",
      isHuman: false,
      isPatientZero: false,
      biteCode: "abc123"
    }
  }

  leaveGame() {
    this.player = undefined;
  }
}
