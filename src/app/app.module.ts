import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LandingPage } from './pages/landing/landing.page';
import { GameDetailsPage } from './pages/game-details/game-details.page';
import { NavbarComponent } from './components/navbar/navbar.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { GameListComponent } from './components/game-list/game-list.component';
import { AdminPage } from './pages/admin/admin.page';
import { MapComponent } from './components/map/map.component';
import {StompService} from "./services/stomp.service";
import {ReactiveFormsModule} from "@angular/forms";
import {JwtInterceptor} from "./services/jwt.interceptor";

@NgModule({
  declarations: [
    AppComponent,
    LandingPage,
    GameDetailsPage,
    NavbarComponent,
    GameListComponent,
    AdminPage,
    MapComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule
  ],
  providers: [StompService, {provide: HTTP_INTERCEPTORS, useClass:JwtInterceptor, multi:true}],
  bootstrap: [AppComponent]
})
export class AppModule { }
