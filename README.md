## Name
HvZ Project
## Repo
https://gitlab.com/kristof.mata.2/hvz-project-frontend
https://gitlab.com/kristof.mata.2/hvz-project-backend

## Run locally

- clone repo
- run ```npm i``` in root
- if you don't have JSON server, install globally: ```npm install -g json-server```
- run JSON server: ```json-server --watch db.json```
- start app: ```ng s```

